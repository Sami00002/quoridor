function moveDownPawn1() {
    if (round) {
        var flag = false;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (board[i][j].valoare == pawn1 && board[i+1][j-1].valoare != wall && board[i + 2][j].valoare != pawn2 && i + 2 < 19) {
                    board[i][j].change(i, j, w, 1);
                    board[i + 2][j].change(i + 2, j, w, pawn1);
                    flag = true;
                    round = false;
                }
                else if (board[i][j].valoare == pawn1 && board[i+1][j-1].valoare != wall && board[i + 2][j].valoare == pawn2 && i + 4 < 19) {
                    board[i][j].change(i, j, w, 1);
                    board[i + 4][j].change(i + 4, j, w, pawn1);
                    flag = true;
                    round = false;
                }
                if (flag == true)
                    break;
            }
        }
    } else alert("It's not your round");

    if(!round && cpu == true) moveCpuPawn();
}

function moveRightPawn1() {
    if (round) {
        var flag = false;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (board[i][j].valoare == pawn1 && board[i+1][j].valoare != wall2 && board[i][j + 2].valoare != pawn2 && j + 2 < 19) {
                    board[i][j].change(i, j, w, 1);
                    board[i][j + 2].change(i, j + 2, w, pawn1);
                    flag = true;
                    round = false;
                }
                else if (board[i][j].valoare == pawn1 && board[i+1][j].valoare != wall2 && board[i][j + 2].valoare == pawn2 && j + 4 < 19) {
                    board[i][j].change(i, j, w, 1);
                    board[i][j + 4].change(i, j + 4, w, pawn1);
                    flag = true;
                    round = false;
                }
                if (flag == true)
                    break;
            }
        }
    } else alert("It's not your round");

    if(!round && cpu == true) moveCpuPawn();
}

function moveLeftPawn1() {
    if (round) {
        var flag = false;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (board[i][j].valoare == pawn1 && board[i+1][j-2].valoare != wall2 && board[i][j - 2].valoare != pawn2 && j - 2 > 0) {
                    board[i][j].change(i, j, w, 1);
                    board[i][j - 2].change(i, j - 2, w, pawn1);
                    flag = true;
                    round = false;
                }
                else if (board[i][j].valoare == pawn1 && board[i+1][j-2].valoare != wall2 && board[i][j - 2].valoare == pawn2 && j - 4 > 0) {
                    board[i][j].change(i, j, w, 1);
                    board[i][j - 4].change(i, j - 4, w, pawn1);
                    flag = true;
                    round = false;
                }
                if (flag == true)
                    break;
            }
        }
    } else alert("It's not your round");

    if(!round && cpu == true) moveCpuPawn();

}

function moveUpPawn1() {
    if (round) {
        var flag = false;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (board[i][j].valoare == pawn1 && board[i-1][j-1].valoare != wall && board[i - 2][j].valoare != pawn2 && i - 2 > 0) {
                    board[i][j].change(i, j, w, 1);
                    board[i - 2][j].change(i - 2, j, w, pawn1);
                    flag = true;
                    round = false;
                }
                else  if (board[i][j].valoare == pawn1 && board[i-1][j-1].valoare != wall && board[i - 2][j].valoare == pawn2 && i - 4 > 0){
                    board[i][j].change(i, j, w, 1);
                    board[i - 4][j].change(i - 4, j, w, pawn1);
                    flag = true;
                    round = false;
                }
                if (flag == true)
                    break;
            }
        }
    } else alert("It's not your round");

    if(!round && cpu == true) moveCpuPawn();

}

function moveUpPawn2() {
    if (!round) {
        var flag = false;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (board[i][j].valoare == pawn2 && pawn1 && board[i-1][j-1].valoare != wall && board[i - 2][j].valoare != pawn1 && i - 2 > 0) {
                    board[i][j].change(i, j, w, 1);
                    board[i - 2][j].change(i - 2, j, w, pawn2);
                    flag = true;
                    round = true;
                }
                else  if (board[i][j].valoare == pawn2 && pawn1 && board[i-1][j-1].valoare != wall && board[i - 2][j].valoare == pawn1 && i - 4 > 0){
                    board[i][j].change(i, j, w, 1);
                    board[i - 4][j].change(i - 4, j, w, pawn2);
                    flag = true;
                    round = true;
                }
                if (flag == true)
                    break;
            }
        }
    } else alert("It's not your round");
}

function moveRightPawn2() {
    if (!round) {
        var flag = false;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (board[i][j].valoare == pawn2 && board[i+1][j].valoare != wall2 && board[i][j + 2].valoare != pawn1 && j + 2 < 19) {
                    board[i][j].change(i, j, w, 1);
                    board[i][j + 2].change(i, j + 2, w, pawn2);
                    flag = true;
                    round = true;
                }
                else if (board[i][j].valoare == pawn2 && board[i+1][j].valoare != wall2 && board[i][j + 2].valoare == pawn1 && j + 4 < 19) {
                    board[i][j].change(i, j, w, 1);
                    board[i][j + 4].change(i, j + 4, w, pawn2);
                    flag = true;
                    round = true;
                }
                if (flag == true)
                    break;
            }
        }
    } else alert("It's not your round");
}

function moveLeftPawn2() {
    if (!round) {
        var flag = false;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (board[i][j].valoare == pawn2 && board[i+1][j-2].valoare != wall2 && board[i][j - 2].valoare != pawn1 && j - 2 > 0) {
                    board[i][j].change(i, j, w, 1);
                    board[i][j - 2].change(i, j - 2, w, pawn2);
                    flag = true;
                    round = true;
                }
                else if (board[i][j].valoare == pawn2 && board[i+1][j-2].valoare != wall2 && board[i][j - 2].valoare == pawn1 && j - 4 > 0) {
                    board[i][j].change(i, j, w, 1);
                    board[i][j - 4].change(i, j - 4, w, pawn2);
                    flag = true;
                    round = true;
                }
                if (flag == true)
                    break;
            }
        }
    } else alert("It's not your round");
}

function moveDownPawn2() {
    if (!round) {
        var flag = false;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (board[i][j].valoare == pawn2 && board[i+1][j-1].valoare != wall && board[i + 2][j].valoare != pawn1 && i + 2 < 19) {
                    board[i][j].change(i, j, w, 1);
                    board[i + 2][j].change(i + 2, j, w, pawn2);
                    flag = true;
                    round = true;
                }
                else if (board[i][j].valoare == pawn2 && board[i+1][j-1].valoare != wall && board[i + 2][j].valoare == pawn1 && i + 4 < 19) {
                    board[i][j].change(i, j, w, 1);
                    board[i + 4][j].change(i + 4, j, w, pawn2);
                    flag = true;
                    round = true;
                }
                if (flag == true)
                    break;
            }
        }
    } else alert("It's not your round");
}

function moveCpuPawn(){
    if(!round && cpu == true){
       let choice1 = 2;

       if(choice1 < 4){
           let direction = floor(random(7))+1;

           console.log(direction);
           if(direction <=4) {
               moveUpPawn2();
               if(!round && cpu) moveCpuPawn();
            }
            if(direction == 5) {
                moveRightPawn2();
                if(!round && cpu) moveCpuPawn();
             }
             if(direction == 6) {
                moveLeftPawn2();
                if(!round && cpu) moveCpuPawn();
             }
             if(direction == 7) {
                moveDownPawn2();
                if(!round && cpu) moveCpuPawn();
             }

       }
    }
}